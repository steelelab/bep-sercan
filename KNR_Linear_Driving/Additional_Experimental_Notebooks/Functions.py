from qutip import *
import numpy as np 
import matplotlib.pyplot as plt 
from ipywidgets import interact
from mpl_toolkits.mplot3d import Axes3D
plt.rcParams["animation.html"] = "jshtml"
from ipywidgets import IntProgress
from IPython.display import display
from matplotlib import animation

def make_wigners(x, t, result):  
    print("Making wigner functions")
    wigner_array = np.empty([len(t),len(x),len(x)])
    f = IntProgress(min=0, max=len(t)-1) # instantiate the bar
    display(f) # display the bar
    for i in range(0, len(t)):
        wig = wigner(result.states[i],x,x)
        wigner_array[i,:,:] = wig
        f.value=i
        
    return wigner_array

def make_animation(x, t, wigner):
    f = IntProgress(min=0, max=len(t)-1) # instantiate the bar
    
    extent = [x[0],x[-1],x[0],x[-1]]
    fig = plt.figure()
    plane = plt.imshow(wigner[0,:,:], cmap = 'RdBu', vmin = -np.max(wigner[0,:,:]), vmax = np.max(wigner[0,:,:]), extent = extent, origin = 'lower');
    plt.ylabel(r'$Im(\alpha)$')
    plt.xlabel(r'$Re(\alpha)$')
    plt.colorbar()
    title = plt.title("Wigner")
    plt.close(fig)
    
    def animate(i):
        if (i == 1):
            print("Generating animation")
            display(f) # display the bar
        wig_i = wigner[i,:,:]
        plane.set_data(wig_i)
        f.value = i
        return plane,
    
    anim = animation.FuncAnimation(fig, animate,frames=len(t), interval = 30)
    
    return anim
